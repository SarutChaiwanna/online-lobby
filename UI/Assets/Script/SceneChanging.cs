﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanging : MonoBehaviour
{
    public void SceneLobby()
    {
        SceneManager.LoadScene ("Lobby");
    }

    public void SceneWaitingRoom()
    {
        SceneManager.LoadScene ("WaitingRoom");
    }
}